from django.test import TestCase,Client

# Create your tests here.
from django.urls import resolve
from django.http import HttpRequest
from django.utils import timezone

from .views import accordion

import time

# Create your tests here.
class LandingPageTest(TestCase):
    def test_apakah_ada_url_landing_page(self):
        response= Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_jika_tidak_ada_url_landing_page(self):
        response= Client().get('/other_url')
        self.assertEqual(response.status_code, 404)

    def test_apakah_ada_template_landing_page(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'nicequery.html')

    def test_fungsi_landing_page(self):
        found= resolve('/')
        self.assertEqual(found.func, accordion)
    
    def test_ada_accordion(self):
        c = Client()
        response = c.get('/')
        content = response.content.decode('utf8')
        self.assertIn('id="accordion"', content)

    def test_ada_aktivitas(self):
        c = Client()
        response = c.get('/')
        content = response.content.decode('utf8')
        self.assertIn('Activities', content)

    def test_ada_organisasi_kepanitiaan(self):
        c = Client()
        response = c.get('/')
        content = response.content.decode('utf8')
        self.assertIn('Experiences', content)

    def test_ada_prestasi(self):
        c = Client()
        response = c.get('/')
        content = response.content.decode('utf8')
        self.assertIn('Achievements', content)

    


    
